package util

import (
	"log"
	"math/big"

	"github.com/shopspring/decimal"
)

func BLessThan(m *big.Int, n *big.Int) bool {
	return m.Cmp(n) < 0
}

func BGreaterThan(m *big.Int, n *big.Int) bool {
	return m.Cmp(n) > 0
}

func BigEqual(m *big.Int, n *big.Int) bool {
	return m.Cmp(n) == 0
}

func BigMul(x, y *big.Int) *big.Int {
	return big.NewInt(0).Mul(x, y)
}

func BigSub(m *big.Int, n *big.Int) *big.Int {
	return big.NewInt(0).Sub(m, n)
}

func BigDiv(x, y *big.Int) *big.Int {
	return big.NewInt(0).Div(x, y)
}

func BigAdd(x, y *big.Int) *big.Int {
	return big.NewInt(0).Add(x, y)
}

func BigAddInt(x *big.Int, i int) *big.Int {
	return big.NewInt(0).Add(x, big.NewInt(int64(i)))
}

func StrToBigIntMust(str string) *big.Int {
	rst, ok := StrToBigInt(str)
	if !ok {
		panic("fail to parse str to bigint:" + str)
	}

	return rst
}

func StrToBigInt(str string) (*big.Int, bool) {
	return big.NewInt(0).SetString(str, 10)
}

func Hex2BigInt(str string) (*big.Int, bool) {
	str = Clean0xPrefixIfExist(str)

	rst := &big.Int{}
	_, ok := rst.SetString(str, 16)
	if !ok {
		log.Println("not ok bigInt.SetString:" + str)
		return nil, false
	}

	return rst, ok
}

func BytesToBigInt(payload []byte) *big.Int {
	rst := &big.Int{}
	rst = rst.SetBytes(payload)

	return rst
}

func BigIntSmaller(a, b *big.Int) *big.Int {
	if a == nil {
		return b
	} else if b == nil {
		return a
	}

	if a.Cmp(b) <= 0 {
		return a
	} else {
		return b
	}
}

func BigIntBigger(a, b *big.Int) *big.Int {
	if a == nil {
		return b
	} else if b == nil {
		return a
	}

	if a.Cmp(b) >= 0 {
		return a
	} else {
		return b
	}
}

func IsPositive(x *big.Int) bool {
	return x.Cmp(big.NewInt(0)) > 0
}

func IsZero(x *big.Int) bool {
	return x.Cmp(big.NewInt(0)) == 0
}

func IsNegative(x *big.Int) bool {
	return x.Cmp(big.NewInt(0)) < 0
}

func MaxUInt256() *big.Int {
	approveAmt := &big.Int{}
	approveAmt.SetString("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff", 16)

	return approveAmt
}

var gwei = big.NewInt(1e9)

func Gwei(n int64) *big.Int {
	return BigMul(big.NewInt(n), gwei)
}
func Eth(n int64) *big.Int {
	return BigMul(big.NewInt(n), big.NewInt(1e18))
}

func Big2Int(b *big.Int) (i int64, safe bool) {
	if b.IsInt64() {
		return b.Int64(), true
	} else {
		return b.Int64(), false
	}
}

func MulBase1e18(a, b *big.Int) *big.Int {
	if a == nil || b == nil {
		return nil
	}

	mul := big.NewInt(0).Mul(a, b)
	return big.NewInt(0).Div(mul, big.NewInt(1e18))
}

func DivBase1e18(a, b *big.Int) *big.Int {
	return DivBase1eN(a, b, 18)
}

func DivBase1eN(a, b *big.Int, n int) *big.Int {
	if a == nil || b == nil {
		return nil
	}

	base := Pow10(n)

	multiplied := big.NewInt(0).Mul(a, base)
	return big.NewInt(0).Div(multiplied, b)
}

func Pow10(n int) *big.Int {
	return big.NewInt(0).Exp(big.NewInt(10), big.NewInt(int64(n)), nil)
}

func EthToWei(f float64) *big.Int {
	return ToWei(f, 18)
}

func ToWei(f float64, decimals int) *big.Int {
	base := decimal.New(1, int32(decimals))
	return decimal.NewFromFloat(f).Mul(base).BigInt()
}

// 1100000000000000000 -> 1.1
func WeiToReadableETH(n *big.Int) float64 {
	return WeiToReadable(n, 18)
}

func WeiToReadable(n *big.Int, decimals int) float64 {
	f, _ := decimal.NewFromBigInt(n, int32(-1*decimals)).Float64()
	return f
}
