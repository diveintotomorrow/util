package util

import (
	"fmt"
	"log"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/stretchr/testify/assert"
)

func TestHexStr2BigInt(t *testing.T) {
	s := "000000000000000000000000000000000000000000000002b5e3af16b1880000000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc200000000000000000000000000000000000000000000012d1d83834c7358e00000000000000000000000000089d24a6b4ccb1b6faa2625fe562bdd9a23260359000000000000000000000000004075e4d4b1ce6c48c81cc940e2bad24b489e64000000000000000000000000000000000000000000000000000000005bfc1059"

	assert.True(t, len(s)/64 == 6)

	for i := 0; i < len(s); i += 64 {
		part := s[i : i+64]
		fmt.Println(part)
	}

	log.Println(s[64*5 : 64*6])
	ts, _ := Hex2BigInt(s[64*5 : 64*6])
	log.Println(ts.Uint64())
}

func TestHexStr2BigIntWith0xPrefix(t *testing.T) {
	s := "0x000000000000000000000000000000000000000000000000000000000002bc55"

	ts, _ := Hex2BigInt(s)
	log.Println(ts)

	assert.Equal(t, ts, big.NewInt(179285))
}

func TestBytes2BigInt(t *testing.T) {
	s := "0x000000000000000000000000000000000000000000000000000000000002bc55"

	bytes, err := hexutil.Decode(s)
	if err != nil {
		panic(err)
	}

	bInt := BytesToBigInt(bytes)

	assert.Equal(t, big.NewInt(179285), bInt)
}

func TestEthToWei(t *testing.T) {
	f := 1.23

	bigInWei := EthToWei(f)

	assert.Equal(t, "1230000000000000000", bigInWei.String())
}

func TestEthToWei2(t *testing.T) {
	f := 4.1

	bigInWei := EthToWei(f)

	assert.Equal(t, "4100000000000000000", bigInWei.String())
}

func TestEthToReadable(t *testing.T) {
	s := big.NewInt(1.1e18)
	r := WeiToReadableETH(s)

	assert.Equal(t, 1.1, r)
}

func TestToWei(t *testing.T) {
	s := 1.1
	bigInWei := ToWei(s, 6)

	assert.Equal(t, "1100000", bigInWei.String())
}
