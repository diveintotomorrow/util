package util

import "github.com/shopspring/decimal"

var GWei = decimal.New(1, 9)
var Dec18 = decimal.New(1, 18)
var Base18 = decimal.New(1, 18)
