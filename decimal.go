package util

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/shopspring/decimal"
)

func Hex2Decimal(hex string) (decimal.Decimal, bool) {
	big, err := hexutil.DecodeBig(hex)
	if err != nil {
		return decimal.Zero, false
	}

	return decimal.NewFromBigInt(big, 0), true
}

func GWeiFromFloat(f float64) decimal.Decimal {
	return decimal.NewFromFloat(f).Mul(GWei)
}

func DecimalFromBigInt(i *big.Int) decimal.Decimal {
	return decimal.NewFromBigInt(i, 0)
}

func Bytes2Decimal(payload []byte) decimal.Decimal {

	rst := &big.Int{}
	rst = rst.SetBytes(payload)

	return BigInt2Decimal(rst)
}

func Decimal2BigInt(d decimal.Decimal) (*big.Int, bool) {
	n := new(big.Int)
	n, ok := n.SetString(d.Truncate(0).StringFixed(0), 10)
	if !ok {
		return nil, false
	}

	return n, true
}

func DecimalSafe2BigInt(dec decimal.Decimal) *big.Int {
	i, ok := Decimal2BigInt(dec)
	if ok {
		return i
	} else {
		panic("fail to DecimalSafe2BigInt(), decimal:" + dec.String())
	}
}

func BigLessThanF(m *big.Int, f float64) bool {
	return decimal.NewFromBigInt(m, 0).LessThan(decimal.NewFromFloat(f))
}
func BigGreaterThanF(m *big.Int, f float64) bool {
	return decimal.NewFromBigInt(m, 0).GreaterThan(decimal.NewFromFloat(f))
}
func BigMulF(x *big.Int, f float64) *big.Int {
	rst := decimal.NewFromBigInt(x, 0).Mul(decimal.NewFromFloat(f))

	intRst := rst.Truncate(0).String()

	rstBig := &big.Int{}
	rstBig.SetString(intRst, 10)

	return rstBig
}

func HumanFriendlyToBigIntWithDecimals(dec decimal.Decimal) *big.Int {
	dec = dec.Mul(decimal.New(1, 18)).Truncate(0)

	rst := &big.Int{}
	_, ok := rst.SetString(dec.String(), 10)
	if !ok {
		panic("fail to HumanFriendlyToBigIntWithDecimals for:" + dec.String())
	}

	return rst
}

func BigInt2Decimal(i *big.Int) decimal.Decimal {
	return decimal.NewFromBigInt(i, 0)
}

//  uint constant WAD = 10 ** 18;
//  uint constant RAY = 10 ** 27;
//
//  function wmul(uint x, uint y) internal pure returns (uint z) {
//      z = add(mul(x, y), WAD / 2) / WAD;
//  }
//  function rmul(uint x, uint y) internal pure returns (uint z) {
//      z = add(mul(x, y), RAY / 2) / RAY;
//  }
//  function wdiv(uint x, uint y) internal pure returns (uint z) {
//      z = add(mul(x, WAD), y / 2) / y;
//  }
//  function rdiv(uint x, uint y) internal pure returns (uint z) {
//      z = add(mul(x, RAY), y / 2) / y;
//  }

var WAD = decimal.New(1, 18)
var RAY = decimal.New(1, 27)

var Two = decimal.NewFromFloat(2)

func WMul(d1, d2 decimal.Decimal) decimal.Decimal {
	return d1.Mul(d2).Add(WAD.Div(Two)).Div(WAD).Truncate(0)
}

func RMul(d1, d2 decimal.Decimal) decimal.Decimal {
	return d1.Mul(d2).Add(RAY.Div(Two)).Div(RAY).Truncate(0)
}
