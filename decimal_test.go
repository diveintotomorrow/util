package util

import (
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
)

func TestDecimal2BigInt(t *testing.T) {
	d := decimal.NewFromFloat(1.654321)

	i := &big.Int{}
	i, ok := i.SetString(d.Truncate(0).StringFixed(0), 10)

	assert.True(t, ok)
	assert.Equal(t, big.NewInt(1), i)
}

func TestDecimal2BigInt2(t *testing.T) {
	d := decimal.NewFromFloat(1.654321)

	i := &big.Int{}
	i, ok := i.SetString(d.StringFixed(0), 10)

	assert.True(t, ok)
	assert.Equal(t, big.NewInt(2), i)
}
