package util

type E struct {
	String string
}
func (e E) Error() string {
	return e.String
}

func NewError(s string) E {
	return E{s}
}
