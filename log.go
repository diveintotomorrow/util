package util

import (
	"io"
	"os"

	"github.com/sirupsen/logrus"
)

func NewLogger(name string) *logrus.Logger {
	log := logrus.New()

	file := GetOrCreateLogFileWithTs(name)
	log.Printf("printing log to file: %s%s", file.Name(), "\n\n")

	multi := io.MultiWriter(file, os.Stdout)
	log.SetOutput(multi)

	// replace fd-2 with our file to get SysErr in file
	// if runtime.GOOS != "darwin" {
	// 	syscall.Dup2(int(file.Fd()), 2)
	// }

	return log
}
