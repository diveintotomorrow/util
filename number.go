package util

import (
	"math"
	"strconv"

	"github.com/sirupsen/logrus"
)

func ParseFloat(s string, decimalPlacesOpt ...int) (float64, error) {
	num, err := strconv.ParseFloat(s, 64)
	if err != nil {
		logrus.Errorf("parse price error: %v", err)
		return 0, err
	}

	if len(decimalPlacesOpt) == 0 {
		return num, nil
	}

	return ToFixedDecimals(num, decimalPlacesOpt[0]), nil
}

func ToFixedDecimals(num float64, decimalPlaces int) float64 {
	multiplier := math.Pow(10, float64(decimalPlaces))
	num = math.Floor(num*multiplier) / multiplier

	return num
}

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
