package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseFloat(t *testing.T) {
	f, err := ParseFloat("1.654321")
	assert.Nil(t, err)
	assert.Equal(t, f, 1.654321)

	f, err = ParseFloat("1.654321", 0)
	assert.Nil(t, err)
	assert.Equal(t, f, 1.0)

	f, err = ParseFloat("1.654321", 1)
	assert.Nil(t, err)
	assert.Equal(t, f, 1.6)

	f, err = ParseFloat("1.654321", 10)
	assert.Nil(t, err)
	assert.Equal(t, f, 1.654321)
}

func TestToFixedDecimals(t *testing.T) {
	num := 1.654321

	f := ToFixedDecimals(num, 0)
	assert.Equal(t, f, 1.0)

	f = ToFixedDecimals(num, 3)
	assert.Equal(t, f, 1.654)

	f = ToFixedDecimals(num, 10)
	assert.Equal(t, f, 1.654321)
}
