package util

import (
	"github.com/ethereum/go-ethereum/common/hexutil"
	"strings"
)

func IsAllZeroHex(hex string) bool {
	hex = strings.ToLower(hex)

	if strings.HasPrefix(hex, "0x") {
		hex = hex[2:]
	}

	for i := 0; i < len(hex); i++ {
		c := hex[i]

		if c != '0' {
			return false
		}
	}

	return true
}

func Prefix0ToFixedLength(str string, length int) string {
	if len(str) >= length {
		return str
	}

	prefix := strings.Repeat("0", length-len(str))

	return prefix + str
}

func Prefix0ToLength64(str string) string {
	return Prefix0ToFixedLength(str, 64)
}

func EqualIgnoreCase(s1, s2 string) bool {
	return strings.ToLower(s1) == strings.ToLower(s2)
}

func Hex2IntMust(hex string) int {
	cleanedHex := cleanLeadingZerosForHex(hex)

	return int(hexutil.MustDecodeUint64(cleanedHex))
}

func cleanLeadingZeros(s string) string {
	for len(s) > 0 && s[0] == '0' {
		s = s[1:]
	}

	return s
}

func cleanLeadingZerosForHex(s string) string {
	if strings.HasPrefix(strings.ToLower(s), "0x") {
		s = s[2:]
	}

	s = cleanLeadingZeros(s)

	return "0x" + s
}
