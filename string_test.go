package util

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsAllZeroHex(t *testing.T) {
	hex := "0x0000000000"
	rst := IsAllZeroHex(hex)
	assert.True(t, rst)

	hex = "0x0000001000"
	rst = IsAllZeroHex(hex)
	assert.False(t, rst)
}

func TestHex2IntMust(t *testing.T) {
	hex := "0x0000012"
	i := Hex2IntMust(hex)
	assert.Equal(t, 18, i)

	hex = "0x0000013"
	i = Hex2IntMust(hex)
	assert.Equal(t, 19, i)
}

