package util

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/shopspring/decimal"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func IsEmptyAddress(adx common.Address) bool {
	for _, b := range adx.Bytes() {
		if b != byte(0) {
			return false
		}
	}

	return true
}

func Post(body, url string) string {
	resp, err := http.Post(url, "application/json", strings.NewReader(body))
	if err != nil {
		log.Printf("Err when Post: %s to %s, err: %s", body, url, err)
		return ""
	}

	bytes, err := ioutil.ReadAll(resp.Body)

	return string(bytes)
}

func PostWithHeaders(body, url string, headerMap map[string]string) (string, error) {
	client := &http.Client{}
	req, err := http.NewRequest("POST", url, bytes.NewReader([]byte(body)))
	if err != nil {
		return "", fmt.Errorf("req fail, err: %s", err)
	}

	for k, v := range headerMap {
		req.Header.Add(k, v)
	}

	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("resp fail, err: %s", err)
	}

	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read resp.Body fail, err: %s", err)
	}

	resp.Body.Close()
	return string(bytes), nil
}

// dynamically decide price
// current impl: ethgasprice
func GetFastGasPrice() *big.Int {
	oneGwei := int64(1000000001)
	fallbackGasPrice := big.NewInt(10 * oneGwei)

	resp, err := http.Get("https://ethgasstation.info/json/ethgasAPI.json")
	if err != nil {
		// static gasman price
		return fallbackGasPrice
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("resp body err:", err)
		return fallbackGasPrice
	}

	var respJson struct {
		Fast float32 `json:"fast"`
	}
	if err := json.Unmarshal(body, &respJson); err != nil {
		log.Printf("unmarshal err: %s, resp: %s", err, string(body))
		return fallbackGasPrice
	}

	fastGwei := int64(respJson.Fast) / 10
	fmt.Println("fast gasPrice from ETHGasStation:", fastGwei)

	if fastGwei > 50 {
		fastGwei = 50
	}

	//if fastGwei > 150 {
	//	log.Println("gasman price too high: %d > 100gwei, set to 100gwei")
	//	fastGwei = 150
	//} else if fastGwei < 88 {
	//	fastGwei = 88
	//}

	return big.NewInt(fastGwei * oneGwei)
}

func GetStaticFastGasPrice() *big.Int {
	oneGwei := int64(1000000001)
	return big.NewInt(1 * oneGwei)
}

// can handle adx str like this
// 0x000000000000000000000000dd27fbfd9e2ebc345140542bab7bd4235172c3b3
func HexStr2Address(hex string) common.Address {
	adx := common.HexToAddress(hex)

	return adx
}

func Clean0xPrefixIfExist(str string) string {
	if strings.HasPrefix(strings.ToLower(str), "0x") {
		str = str[2:]
	}

	return str
}


func GetOrCreateLogFileWithTs(name string) *os.File {
	var time = time.Now().Format("2006-01-02_15-04")
	name = name + "_" + time

	logFileDirectory := os.Getenv("LOG_FILE_DIR")
	if logFileDirectory == "" {
		logFileDirectory = "./logs"
	}

	os.MkdirAll(logFileDirectory, os.ModePerm)

	var filename = logFileDirectory + fmt.Sprintf("/%s.log", name)
	file, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND|os.O_SYNC, 0666)
	if err != nil {
		log.Fatalln("Failed to open log file:", logFileDirectory, ", err:", err)
	}

	return file
}

func IntTo32bytes(num int) []byte {
	return IntToXBytes(num, 32)
}

func IntToXBytes(num, x int) []byte {
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, uint64(num))

	rst := BytesToXBytes(bs, x)

	return rst
}

func BigIntTo32bytes(i *big.Int) []byte {
	return BytesTo32bytes(i.Bytes())
}

func DecimalTo32bytes(d decimal.Decimal) []byte {
	b := DecimalSafe2BigInt(d)
	return BigIntTo32bytes(b)
}

func BoolTo32Bytes(b bool) []byte {
	tmp := make([]byte, 32)

	if b {
		tmp[31] = 1
	}

	return tmp
}

// [0000...DATA]
func BytesTo32bytes(payload []byte) []byte {
	return BytesToXBytes(payload, 32)
}

func BytesToXBytes(payload []byte, x int) []byte {
	len := len(payload)
	if len > x {
		panic(fmt.Sprintf("fail for bytes len > %d", x))
	}

	tmp := make([]byte, x)
	copy(tmp[x-len:], payload)

	return tmp
}

func BytesTo32byteArray(payload []byte) [32]byte {
	len := len(payload)
	if len > 32 {
		panic("fail for bytes len > 32")
	}

	var tmp [32]byte
	copy(tmp[32-len:], payload)

	return tmp
}

// [DATA...DATA]
// [DATA...0000]
func BytesSuffix0ToMulti32Bytes(payload []byte) []byte {
	var rst []byte

	var i = 0

	// take 32 bytes and append
	for i+31 <= len(payload)-1 {
		part := payload[i : i+32]
		rst = append(rst, part...)

		i = i + 32
	}

	// remaining
	if i <= len(payload)-1 {
		tmp := make([]byte, 32)
		size := len(payload) - i

		copy(tmp[0:size], payload[i:])

		rst = append(rst, tmp...)
	}

	return rst
}

//  input: [ 123...123   123...123]
// output: [[123...123],[123...123]]
func BytesBreakIntoGroupsPer32Bytes(payload []byte) (groups [][32]byte) {
	if len(payload)%32 != 0 {
		panic(fmt.Sprintf("len mod 32 != 0, len: %d", len(payload)))
	}

	for i := 0; i < len(payload); i += 32 {
		var curGroup [32]byte
		copy(curGroup[0:32], payload[i:i+32])

		groups = append(groups, curGroup)
	}

	return
}

// padding at tail
func BytesSuffixTo32bytes(payload []byte) []byte {
	len := len(payload)
	if len > 32 {
		panic("fail for len > 32")
	}

	tmp := make([]byte, 32)
	copy(tmp[0:len], payload)

	return tmp
}

func IsProfitableEarningDai(daiToArbitrage *big.Int, askPrice decimal.Decimal, bidPrice decimal.Decimal) bool {
	panic("IsProfitableEarningDai need to be impled")

	//daiAmtForHuman := decimal.NewFromBigInt(daiToArbitrage, -18)

	// profit = amtOfDaiToArbitrage * (PriceSell / PriceBuy - 1) - Gas
	//grossProfit := daiAmtForHuman.Mul(bidPrice.Div(askPrice).Sub(decimal.NewFromFloat(1.0)))

	//return grossProfit.GreaterThan(config.Current.GasCostEstimateInDaiForHuman)
	return false
}

// 25w * 10Gwei
func IsProfitableEarningEth(ethWeEarn *big.Int) bool {
	gasCost := BigMulF(Gwei(6), 220000)

	if BLessThan(ethWeEarn, gasCost) {
		return false
	} else {
		return true
	}
}

func HexStrTo64CharsInGroup(str string) []string {
	str = Clean0xPrefixIfExist(str)

	if len(str)%64 != 0 {
		return []string{}
	}

	var rst []string
	for i := 0; i < len(str); i += 64 {
		part := str[i : i+64]

		rst = append(rst, part)
	}

	return rst
}

func EarnDaiFlagIsOn() bool {
	return os.Getenv("EARN_DAI_FLAG") == "true"
}

func TransDaiFlagIsOn() bool {
	return os.Getenv("TRANSFER_DAI") == "true"
}

func StripWhiteSpaces(str string) string {
	return strings.Join(strings.Fields(str), "")
}

func IsEmptyBlockHash(blockHash string) bool {
	return blockHash == "0x0000000000000000000000000000000000000000000000000000000000000000"
}

func CreateContextCanHearSig() (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(context.Background())

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	go func() {
		sig := <-gracefulStop
		fmt.Printf("caught sig: %+v", sig)
		cancel()

		fmt.Println("Wait for 5 second to finish processing", time.Now())
		time.Sleep(5 * time.Second)
		fmt.Println("finish waiting", time.Now())

		os.Exit(0)
	}()

	return ctx, cancel
}
