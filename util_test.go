package util

import (
	"encoding/hex"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"log"
	"math/big"
	"testing"
)

func TestHexStrTo64CharsInGroup(t *testing.T) {
	s := "0x000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc200000000000000000000000089d24a6b4ccb1b6faa2625fe562bdd9a232603590000000000000000000000000000000000000000000000046b6de3b5f52e600000000000000000000000000000000000000000000000028d393e77a6f42ca000000000000000000000000000000000000000000000000000000000005c1f00ee"

	groups := HexStrTo64CharsInGroup(s)

	for _, s := range groups {
		log.Println(s)
		assert.Equal(t, len(s), 64)
	}
}
func TestHexStrToAddress(t *testing.T) {
	str := "0x000000000000000000000000dd27fbfd9e2ebc345140542bab7bd4235172c3b3"
	adx := HexStr2Address(str)
	log.Println(adx.String())

	expectedAdx := common.HexToAddress("0xdd27fbfd9e2ebc345140542bab7bd4235172c3b3")

	assert.Equal(t, expectedAdx, adx)
}

func TestSetNilForDecimal(t *testing.T) {
	// will panic
	assert.Panics(t, func() {
		log.Println("panic here")
		big.NewInt(0).Set(nil)
	})
}

func TestPrefix0(t *testing.T) {
	str := "41c73636dc0209cb95a1a7fe81ac8c3c293cfa9dc4"
	expected := "000000000000000000000041c73636dc0209cb95a1a7fe81ac8c3c293cfa9dc4"

	assert.Equal(t, expected, Prefix0ToLength64(str))
}

func TestBytesBreakIntoGroupsPer32Bytes(t *testing.T) {
	s := "000000000000000000000000c02aaa39b223fe8d0a0e5c4f27ead9083c756cc200000000000000000000000089d24a6b4ccb1b6faa2625fe562bdd9a232603590000000000000000000000000000000000000000000000046b6de3b5f52e600000000000000000000000000000000000000000000000028d393e77a6f42ca000000000000000000000000000000000000000000000000000000000005c1f00ee"

	bytes, err := hex.DecodeString(s)
	if err != nil {
		panic(err)
	}

	groups := BytesBreakIntoGroupsPer32Bytes(bytes)
	assert.Equal(t, len(groups), 5)

	last32Bytes, _ := hex.DecodeString("000000000000000000000000000000000000000000000000000000005c1f00ee")

	//log.Println(last32Bytes)
	//log.Println(groups[4][:])
	assert.Equal(t, last32Bytes, groups[4][:])
}

func TestIntToXBytes(t *testing.T) {
	bytes := IntToXBytes(1, 12)
	log.Println(hex.EncodeToString(bytes))

	expected := "000000000000000000000001"

	assert.Equal(t, expected, hex.EncodeToString(bytes))
}

func TestBytesSuffixTo32bytes(t *testing.T) {
	bytes := IntToXBytes(1, 12)

	bytes32 := BytesSuffixTo32bytes(bytes)
	log.Println(hexutil.Encode(bytes32))
	log.Println(hex.EncodeToString(bytes32))

	expected := "0000000000000000000000010000000000000000000000000000000000000000"
	assert.Equal(t, expected, hex.EncodeToString(bytes32))
}

func TestNonPointerFieldInStruct(t *testing.T) {
	type Tmp struct {
		Dec decimal.Decimal
	}

	tmp := Tmp{Dec: decimal.NewFromFloat(1.0)}
	tmp.Dec = decimal.NewFromFloat(2.0)

	assert.Equal(t, decimal.NewFromFloat(2.0), tmp.Dec)
}

type TestStruct struct {
	Count int
}

func TestStructInMap(t *testing.T) {
	m := make(map[int]TestStruct)

	m[1] = TestStruct{1}

	cnt := m[1]
	cnt.Count++

	assert.Equal(t, 1, m[1].Count)
}

func TestStructInMap2(t *testing.T) {
	m := make(map[int]*TestStruct)

	m[1] = &TestStruct{1}

	cnt := m[1]
	cnt.Count++

	assert.Equal(t, 2, m[1].Count)
}
